#include "../../include/main.h"
#include "../../include/arm.h"

void armSet(signed char y)
{
    y = (float) y;
    motorSet(MOTOR_A_L_1, y/5);
    motorSet(MOTOR_A_R_1, -y/5);
}
