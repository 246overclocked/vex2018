#include "../../include/main.h"
#include "../../include/drive.h"
#include <math.h>

void driveSet(signed char x, signed char y)
{
    motorSet(MOTOR_D_L_1, y + x);
    motorSet(MOTOR_D_L_2, y + x);
    motorSet(MOTOR_D_R_1, y - x);
    motorSet(MOTOR_D_R_2, y - x);
} 

void driveSetPid(signed char x, signed char y) 
{
    float angleOfAttack;
    angleOfAttack = atan(y/x);    


}

int getDiff(int tick1, int tick2)
{
    int diff;
    if(tick2 < tick1)
    {
        diff = 360 + (tick1 - tick2);
    } else {
        diff = tick1 - tick2;
    }
    
    return diff;
}
