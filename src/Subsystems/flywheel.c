#include "../../include/main.h"
#include "../../include/flywheel.h"

void spinFlyWheel()
{
    motorSet(MOTOR_F_L_1, -127);
    motorSet(MOTOR_F_R_1, -127);
}

void stopFlyWheel(){
	motorStop(MOTOR_F_L_1);
	motorStop(MOTOR_F_R_1);
}
